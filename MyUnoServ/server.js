require('dotenv').config();
const express = require('express');
const http = require('http');
const cors = require('cors');
const lib = require('./functions.js');

var app = express();
app.use(cors());
app.set('port', process.env.PORT);
const server = http.createServer(app);
const io = require('socket.io')(server);

//Debug address
if(process.env.DEV_MODE == 'true'){
  app.get('/', (req, res) => {
    res.send(lib.getParties());
  });
}

app.get('/game/:idGame', function(req, res) {
  res.send(lib.getDataPartie(req.params.idGame));
});

io.on('connection',(socket)=>{
  socket.on('join', function(data, respondFct){
    lib.joinOrCreateGame(socket, io, data, respondFct);
  });

  socket.on('disconnecting', () => {
    if(socket.rooms.size > 1)
      lib.leaveGame(socket, io);
  });

  socket.on('startGame', function(respondFct){
    lib.startGame(socket, io, respondFct);
  });

  socket.on('play', function(card, respondFct){
    lib.playCard(socket, io, card, respondFct);
  });

  socket.on('pickCard', function(respondFct){
    lib.pickCard(socket, io, respondFct);
  });

  socket.on('restart', function(respondFct){
    lib.restart(socket, io, respondFct);
  });
});

server.listen(process.env.PORT);
console.log("API started at "+process.env.PORT);
