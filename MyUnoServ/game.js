const COLORS = ['red', 'green', 'blue', 'yellow'];
const SPECIAL_CARDS = ["+4", "joker"]

class Card{
  constructor(nb, color=null){
    this.special = SPECIAL_CARDS.includes(nb);
    this.nb = nb;
    this.color = color;
  }
}

Card.prototype.toString = function(){
  if (this.special && !this.color)
    return this.nb;
  return `${this.nb} ${this.color}`;
}

class Player{
  constructor(username){
    this.username = username;
    this.deck = [];
  }

  addCard(c){
    this.deck.push(c);
  }

  haveCard(c){
    return (this.deck.indexOf(c) != -1);
  }

  removeCard(c){
    let i = this.deck.indexOf(c);
    if(i != -1)
      this.deck.splice(i, 1);
  }

  clearCard(){
    this.deck.length = 0;
  }

  haveWon(){
    return this.deck.length == 0;
  }
}

class Party{
  constructor(deckSize){
    this.DECK_SIZE = deckSize;
    this.DECK = [];
    this.STACK = [];
    this.players = [];
    this.round = 0;
    this.clockwise = true;
    this.end = false;
  }

  addPlayer(username){
    if(this.players.length<10 && this.players.find(p=>p.username == username) == null){
      this.players.push(new Player(username));
      return true;
    }
    return false;
  }

  removePlayer(username){
    let i = this.players.findIndex(p => (p.username == username));
    if(i != -1){
      this.players.splice(i, 1);
      return true;
    }
    return false;
  }

  generateDeck(){
    for(let c of COLORS){
      for(let i=0; i<2; i++){
        for(let i=0; i<10; i++){ // create num
          this.DECK.push(new Card(`${i}`, c));
        }
        // create colored specials
        this.DECK.push(new Card("+2", c));
        this.DECK.push(new Card("swap", c));
        this.DECK.push(new Card("block", c));
      }
      // create specials
      for(let el of SPECIAL_CARDS){
        this.DECK.push(new Card(el));
      }
    }

    // Fisher–Yates shuffle to the deck
    for (let i = this.DECK.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      let x = this.DECK[i];
      this.DECK[i] = this.DECK[j];
      this.DECK[j] = x;
    }
  }

  startParty(){
    if(this.players.length < 2)
      return false;

    this.generateDeck();

    // distribute cards to players
    for(let i=0; i<this.DECK_SIZE; i++){
      for(let p of this.players){
        p.addCard(this.DECK.pop());
      }
    }
    // show first card (not a special for start)
    do {
      this.STACK.push(this.DECK.pop());
    } while (this.STACK[this.STACK.length-1].special);

    return true;
  }

  resetGame(){
    for(let el of this.players)
      el.clearCard();

    this.DECK = [];
    this.STACK = [];
    this.round = 0;
    this.clockwise = true;
    this.end = false;

    this.startParty();
  }

  getStackCard(){
    return this.STACK[this.STACK.length-1];
  }

  testCard(playedCard){
    if(playedCard.special)
      return true;

    let stackCard = this.getStackCard();
    return (playedCard.nb == stackCard.nb || playedCard.color == stackCard.color)
  }

  changePlayer(){
    if(this.clockwise)
      this.round = (this.round+1) % this.players.length;
    else{
      if(this.round != 0)
        --this.round;
      else
        this.round = this.players.length - 1;
    }
  }

  pickWill(){
    let nb = 0;
    let card;
    do {
      card = this.DECK.pop();
      this.players[this.round].addCard(card);
      nb++;
    } while (!this.testCard(card));
    return nb
  }

  nextPlayerPick(nb){
    let player;
    if(this.clockwise) player = (this.round+1) % this.players.length;
    else player = (this.round != 0) ? this.round-1 : this.players.length - 1;

    for(let i=0; i<nb; i++){
      this.players[player].addCard(this.DECK.pop());
    }
  }

  play(username, card){
    if(!this.end && this.players[this.round].username == username){
      if(this.players[this.round].haveCard(card)){
        if(this.testCard(card)){
          this.players[this.round].removeCard(card);
          this.STACK.push(card);

          if(this.players[this.round].haveWon()){
            this.end = true;
            return true;
          }

          switch(card.nb){
            case "swap":
              this.clockwise = !this.clockwise;
              break;
            case "block":
              this.changePlayer();
              break;
            case "+2":
              this.nextPlayerPick(2);
              break;
            case "+4":
              this.nextPlayerPick(4);
              break;
          }
          this.changePlayer();

          if(this.DECK.length < 2){
            let tmp = this.STACK.slice(0, this.STACK.length-3);
            for(let e of tmp){
              if(e.special){
                e.color = null;
              }
              this.DECK.unshift(e);
            }
          }
          return true;
        }
      }
    }
    return false;
  }
}

module.exports = Party;
