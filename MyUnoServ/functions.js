const Party = require("./game.js");

const PARTIES = {};

function getParties(){
  return PARTIES;
}

function getDataPartie(idGame){
  if(idGame in PARTIES)
    return {
      "idGame": idGame,
      "players": PARTIES[idGame].players.map(p => p.username),
      "start": PARTIES[idGame].STACK.length != 0,
      "end": PARTIES[idGame].end
    }
  return null;
}

function getDataFromSocket(socket){
  for(let room of socket.rooms){
    let data = room.split(":");
    if(data.length > 1){
      return {
        "idGame": data[0],
        "party": PARTIES[data[0]],
        "username": data[1]
      }
    }
  }
  return null;
}

function sendDeckToAll(io, idGame, msgMy, msgOther){
  let party = PARTIES[idGame];
  for(let player of party.players){
    io.to(`${idGame}:${player.username}`).emit("game", {
      "code": msgMy,
      "deck": player.deck
    });
    io.to(idGame).emit("game", {
      "code": msgOther,
      "friends_username": player.username,
      "friends_nbCard": player.deck.length,
      "player_round": party.players[party.round].username
    });
  }
  io.to(idGame).emit("game", {
    "code": "STACK_CARD",
    "card": party.getStackCard(),
    "round": party.players[party.round].username
  });
}

function joinOrCreateGame(socket, io, data, respondFct){
  let ok = data.idGame in PARTIES;
  if(ok){
    if(!PARTIES[data.idGame].addPlayer(data.username)){
      respondFct(false);
      return;
    }
  }
  else{
    PARTIES[data.idGame] = new Party(process.env.NB_CARD_START);
    PARTIES[data.idGame].addPlayer(data.username);
  }

  io.to(data.idGame).emit("game", {
    "code":"JOIN",
    "username": data.username
  });
  respondFct(true);
  socket.join(`${data.idGame}:${data.username}`);
  socket.join(data.idGame);
}

function leaveGame(socket, io){
  let data = getDataFromSocket(socket);
  if(data.party.removePlayer(data.username)){
    if(data.party.players.length == 0){
      delete PARTIES[data.idGame];
    }
    else{
      io.to(data.idGame).emit("game", {
        "code":"LEAVE",
        "username": data.username
      });
    }
  }
}

function startGame(socket, io, respondFct){
  let data = getDataFromSocket(socket);
  if(data.party.players[0].username == data.username && data.party.startParty()){
    sendDeckToAll(io, data.idGame, "START_MY", "START_OTHER");
    respondFct(true);
  }
  else respondFct(false);
}

function playCard(socket, io, data, respondFct){
  let dataSocket = getDataFromSocket(socket);
  let card = dataSocket.party.players[dataSocket.party.round].deck[data.index];

  if(card == null){
    respondFct(false);
    return;
  }

  if(card.special) card.color = data.color;

  if(!dataSocket.party.play(dataSocket.username, card)){
      respondFct(false);
  }
  else{
    respondFct(true);
    sendDeckToAll(io, dataSocket.idGame, "MY_DECK", "OTHER_DECK");

    if(dataSocket.party.end){
      io.to(dataSocket.idGame).emit("game", {
        "code":"WIN",
        "user": dataSocket.username
      });
    }
  }
}

function pickCard(socket, io, respondFct){
  let data = getDataFromSocket(socket);
  if(data.party.players[data.party.round].username == data.username){
    let nb = data.party.pickWill();
    respondFct(nb);
    sendDeckToAll(io, data.idGame, "MY_DECK", "OTHER_DECK");
  }
  else respondFct(0);
}

function restart(socket, io, respondFct){
  let data = getDataFromSocket(socket);
  if(data.party.end && data.party.players[0].username == data.username){
    data.party.resetGame();
    respondFct(true);
    sendDeckToAll(io, data.idGame, "RESTART_MY", "RESTART_OTHER");
  }
  else respondFct(false);
}

module.exports = {
  joinOrCreateGame,
  getParties,
  getDataPartie,
  leaveGame,
  startGame,
  playCard,
  pickCard,
  restart
};
