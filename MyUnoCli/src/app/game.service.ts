import { Injectable } from '@angular/core';
import { io } from 'socket.io-client';
import { environment as env } from '../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private socket: any;

  constructor(private http: HttpClient, private msgService: MessageService) {
    this.socket = io();
  }

  joinRoom(idGame: string, username: string): Promise<boolean>{
    let data = {
      idGame: idGame,
      username: username
    };

    return new Promise(resolve=>{
      this.socket.emit('join', data, (res: boolean)=>{
        if(res){
          this.msgService.add({severity:'success', summary:'Partie rejointe', detail:'La partie a été rejointe avec succès.'});
        }
        else{
          this.msgService.add({severity:'error', summary:'Partie non rejointe', detail:'Pseudo déjà utilisé ou place manquante.'});
        }
        resolve(res);
      })
    });
  }

  startGame() {
    this.socket.emit('startGame', (res: boolean)=>{
      if(res){
        this.msgService.add({severity:'success', summary:'Partie lancée', detail:'La partie a été lancé avec succès.'});
      }
      else{
        this.msgService.add({severity:'error', summary:'Partie non lancée',
            detail:"Soit vous n'êtes pas l'administrateur soit il manque un joueur adverse."});
      }
    });
  }

  getGameObservable(): Observable<any>{
    return new Observable<any>((observer)=>{
      this.socket.on("game", (data : any)=>{
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      }
    });
  }

  playCard(cardIndex: number, color:string|null){
    let data: any;
    if(color == null)
      data = {"index": cardIndex}
    else
      data = {"index": cardIndex, "color": color}

    this.socket.emit('play', data, (res: boolean)=>{
      if(!res){
        this.msgService.add({severity:'error', summary:'Carte non jouée',
            detail:"Ce n'est pas votre tour de jouer ou votre carte ne peut pas être joué actuellement."});
      }
    });
  }

  pickCard(){
    this.socket.emit('pickCard', (res: number)=>{
      if(res>=1){
        this.msgService.add({severity:'success', summary:'Carte(s) piochée(s)',
            detail:`Vous avez pioché ${res} carte(s) pour pouvoir jouer.`});
      }
      else{
        this.msgService.add({severity:'error', summary:'Carte non piochée',
            detail:"Ce n'est pas votre tour de jouer, vous ne pouvez donc pas piocher."});
      }
    });
  }

  restart(){
    this.socket.emit('restart', (res: boolean)=>{
      if(res){
        this.msgService.add({severity:'success', summary:'Partie redémarrée',
            detail:`Vous avez la partie.`});
      }
      else{
        this.msgService.add({severity:'error', summary:'Partie non redémarrée',
            detail:"Soit vous n'êtes pas l'administrateur soit la partie n'est pas encore finie."});
      }
    });
  }

  getDataParty(idGame: string): Promise<any>{
    let JSON_HEADER = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
    return this.http.get<any>("/api/game/"+idGame, JSON_HEADER).toPromise();
  }

}
