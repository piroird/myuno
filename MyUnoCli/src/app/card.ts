const COLORS = ['red', 'green', 'blue', 'yellow'];
const SPECIAL_CARDS = ["+4", "joker"]

export class Card{
  special: boolean;
  nb: string;
  color: string;

  constructor(nb: string, color:string = ""){
    this.special = SPECIAL_CARDS.includes(nb);
    this.nb = nb;
    this.color = color;
  }

  static deserialize(input: any): Card {
    return new Card(input.nb, input.color);// Object.assign(this, input);
  }

  static getColors(): any[]{
    return [
      {"label":"Rouge", "code":"red"},
      {"label":"Vert", "code":"green"},
      {"label":"Bleu", "code":"blue"},
      {"label":"Jaune", "code":"yellow"}
    ]
  }

  public setColor(color:string): boolean{
    if(this.special && COLORS.includes(color)){
      this.color = color;
      return true;
    }
    return false;
  }
}
