import { Component, OnInit, AfterViewInit, ElementRef, ViewChild  } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('gameInput') gameInput!: ElementRef;
  idGame: string = "";

  constructor(private router: Router, private msgService: MessageService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    this.gameInput.nativeElement.focus();
  }

  keyPressVerification(event: any): boolean{
    if (/[a-zA-Z0-9]/.test(event.key)) {
      return true;
    }
    else {
      this.msgService.add({severity:'error', summary:'Erreur de caractère',
          detail:"Seuls les caractères alphanumériques sont autorisés."});
      return false;
    }
  }

  randomName(){
    this.idGame = ""
    let storedCharacters =
        '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i=0; i<5; i++)
      this.idGame += storedCharacters.charAt(
        Math.floor(Math.random()*storedCharacters.length));
  }

  joinRoom(){
    if(this.idGame != "")
      this.router.navigateByUrl(`/game/${this.idGame}`);
    else
    this.msgService.add({severity:'error', summary:'Nom invalide',
        detail:"Un nom de salle doit être entré."});
  }
}
