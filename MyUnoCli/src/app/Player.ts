export interface Player{
  username: string;
  deckSize?: number;
}
