import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GameService } from '../game.service';
import { Card } from "../card";
import { Player } from "../Player";
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { Router } from '@angular/router';
import { Message } from 'primeng/api';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.sass']
})
export class GameComponent implements OnInit, AfterViewInit {
  @ViewChild('usernameInput') usernameInput!: ElementRef;
  state = 1;
  idGame!: string;
  username = "";
  friends: Player[] = [];
  myDeck!: Card[];
  stackCard!: Card;
  playerRound!: string;
  canRestart = false;
  joinRoomDisabled = false;
  colors = Card.getColors();
  selectedColor!: string;
  selectedCardIndex!: number;
  displayChoiceColor = false;

  constructor(private route: ActivatedRoute, private gameService: GameService, private router: Router,
    private msgService: MessageService, private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if(params.id.indexOf(":") == -1){
        this.idGame = params.id;
        this.gameService.getDataParty(this.idGame).then((data: any) => {
          if(data != null)
            this.friends = data.players.map((p: any) => {return {username: p}});
        });
      }
      else this.router.navigateByUrl("/home");
    });

    this.gameService.getGameObservable().subscribe((data: any) => {
      switch (data.code){
        case "JOIN":
          if(data.username != this.username)
            this.friends.push({username: data.username});
          break;

        case "LEAVE":
          this.friends.splice(this.friends.findIndex(f => f.username == data.username), 1);
          break;

        case "START_MY":
        case "RESTART_MY":
          this.state = 2;
          this.msgService.clear();
          this.myDeck = data.deck.map((el:any) => Card.deserialize(el));
          break;

        case "MY_DECK":
          this.myDeck = data.deck.map((el:any) => Card.deserialize(el));
          break;

        case "STACK_CARD":
          this.stackCard = data.card;
          this.playerRound = data.round;
          break;

        case "START_OTHER":
        case "RESTART_OTHER":
        case "OTHER_DECK":
          let p = this.friends.find((p: any) => p.username == data.friends_username);
          if(p != null) p.deckSize = data.friends_nbCard;
          else if(data.friends_username != this.username) this.friends.push(
              {username: data.friends_username, deckSize: data.friends_nbCard});
          break;

        case "WIN":
          if(data.user != this.username)
            this.msgService.add({severity:'error', summary:'Défaite', detail:`Victoire de ${data.user}.`, life:30000000});
          else
            this.msgService.add({severity:'success', summary:'Victoire', detail:"Bravo, vous avez gagné cette partie !", life:30000000});
          this.canRestart = true;
          break;

        default:
          console.log(data);
      }
    });
  }

  ngAfterViewInit(){
    this.usernameInput.nativeElement.focus();
  }

  keyPressVerification(event: any): boolean{
    if (/[a-zA-Z0-9-_éèàç@&+=$ùµ]/.test(event.key)) {
      return true;
    }
    else {
      this.msgService.add({severity:'error', summary:'Erreur de caractère',
          detail:"Les seuls caractères spéciaux autorisé sont :\n- _ é è ç à @ & + = $ ù µ"});
      return false;
    }
  }

  joinRoom(){
    if(this.username != null && this.username != ""){
      this.joinRoomDisabled = true;
      this.gameService.joinRoom(this.idGame, this.username).then((ok: boolean)=>{
          this.joinRoomDisabled = ok;
      });
    }
  }

  startGame(event: Event){
    this.confirmationService.confirm({
        target: (event.target != null) ? event.target : undefined,
        message: 'Êtes-vous sûr de vouloir lancer la partie ?',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: "Oui",
        rejectLabel: "Non",
        acceptButtonStyleClass: "p-button-success",
        rejectButtonStyleClass: "p-button-danger",
        accept: () => {
          this.gameService.startGame();
        }
    });
  }

  playCard(cardIndex: number){
    if(this.myDeck[cardIndex].special){
        this.displayChoiceColor = true;
        this.selectedCardIndex = cardIndex;
    }
    else
      this.gameService.playCard(cardIndex, null);
  }

  validateColor(){
    this.displayChoiceColor = false;
    this.gameService.playCard(
      this.selectedCardIndex as number,
      this.selectedColor as string
    );
  }

  pickCard(){
    this.gameService.pickCard();
  }

  restart(){
    this.canRestart = false;
    this.gameService.restart();
  }
}
