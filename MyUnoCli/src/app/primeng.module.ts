import { NgModule } from '@angular/core';

import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DividerModule} from 'primeng/divider';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {ConfirmationService} from 'primeng/api';
import {DialogModule} from 'primeng/dialog';
import {SelectButtonModule} from 'primeng/selectbutton';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {MessageModule} from 'primeng/message';

@NgModule({
  imports: [
    ButtonModule,
    InputTextModule,
    DividerModule,
    ToastModule,
    ConfirmPopupModule,
    DialogModule,
    SelectButtonModule,
    ScrollPanelModule,
    MessageModule
  ],
  exports: [
    ButtonModule,
    InputTextModule,
    DividerModule,
    ToastModule,
    ConfirmPopupModule,
    DialogModule,
    SelectButtonModule,
    ScrollPanelModule,
    MessageModule
  ],
  providers: [MessageService, ConfirmationService]
})
export class PrimengModule { }
