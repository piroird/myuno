# Descriptions
Création d'un jeu de UNO en ligne en temps réel.

# Techno
Front-end
- [Angular](https://angular.io/)
  - [PrimeNG](https://primefaces.org/primeng/)

Back-end
- [Node.js](https://nodejs.org/en/)
  - [express](https://expressjs.com/)
  - [socket.io](https://socket.io/)

# Images
Les images des cartes proviennent de [la page Wikipédia du jeu](https://www.wikiwand.com/en/Uno_(card_game)).
